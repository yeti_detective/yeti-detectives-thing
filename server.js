const express = require("express");
const path = require("path");

const PORT = process.env.PORT || 8080;

const app = express();

app.get("/img", (req, res) => {
  res.sendFile(path.join(__dirname, "cat_man.png"));
});

app.get("/", (req, res) => {
  console.log(req.headers);
  res.sendFile(path.join(__dirname, "index.html"));
});

app.listen(PORT, () => {
  console.log(`listening on port ${PORT}`);
});
